FROM golang:1.15.4-buster as builder
# install xz
RUN apt-get update && apt-get install -y \
    xz-utils \
&& rm -rf /var/lib/apt/lists/*
RUN apt-get install -y ca-certificates
# install UPX
ADD https://github.com/upx/upx/releases/download/v3.96/upx-3.96-amd64_linux.tar.xz /usr/local
RUN xz -d -c /usr/local/upx-3.96-amd64_linux.tar.xz | \
    tar -xOf - upx-3.96-amd64_linux/upx > /bin/upx && \
    chmod a+x /bin/upx
# enable go module
ENV GO111MODULE=on
# create a working directory
WORKDIR /go/src/gitlab.com/randomtext/rabbitmq-api
# add go.mod and go.sum
ADD go.mod go.mod
ADD go.sum go.sum
ADD .git .git
# copy dependencies into the vendor folder
RUN go mod download
# add source code
ADD src src
# build the source
RUN CGO_ENABLED=0 GOOS=linux go build -a -o main src/main.go
RUN upx main
FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
# set working directory
WORKDIR /root
# copy the binary from builder
COPY --from=builder /go/src/gitlab.com/randomtext/rabbitmq-api/main .
# run the binary
CMD ["./main"]