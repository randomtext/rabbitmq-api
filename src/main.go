package main

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/streadway/amqp"
	connections2 "gitlab.com/random-text/rabbitMQ/src/connections"
	"gitlab.com/random-text/rabbitMQ/src/server/delivery/rabbitmq"
	"gitlab.com/random-text/rabbitMQ/src/server/repository"
	"gitlab.com/random-text/rabbitMQ/src/server/usecase"
	"go.mongodb.org/mongo-driver/mongo"
	"os"
)

func initEndpoints(mongoC *mongo.Client, rabbitmqCh *amqp.Channel, rabbitmqQueue amqp.Queue, rdb *redis.Client) {
	r := repository.New(mongoC)
	uc := usecase.New(r, rdb)
	_ = rabbitmq.New(uc, rabbitmqCh, rabbitmqQueue)
}

func main() {
	mongoC, err := connections2.MongoInit()
	if err != nil {
		panic(err)
	}
	rabbitmqC, rabbitmqCh, err := connections2.RabbitMQConnect()
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := mongoC.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
		if err := rabbitmqC.Close(); err != nil {
			panic(err)
		}
		if err := rabbitmqCh.Close(); err != nil {
			panic(err)
		}
	}()
	rabbitmqQueue := connections2.RabbitMQReceive(rabbitmqCh)
	rdb := connections2.RedisConnect()
	initEndpoints(mongoC, rabbitmqCh, rabbitmqQueue, rdb)
	fmt.Println("rabbitmq is running in development mode on port " + os.Getenv("RABBITMQ_PORT"))
	fmt.Println("redis is running in development mode on port " + os.Getenv("REDIS_PORT"))
	forever := make(chan bool)
	<-forever
}
