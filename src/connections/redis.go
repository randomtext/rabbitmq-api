package connections

import (
	"fmt"
	"github.com/go-redis/redis/v8"
	"os"
)

func RedisConnect() *redis.Client {
	url := fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT"))
	rdb := redis.NewClient(&redis.Options{
		Addr:     url,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	return rdb
}
