package utils

import (
	"encoding/json"
	systemError "errors"
	ut "github.com/go-playground/universal-translator"
	"net/http"
	"os"
	"strings"

	"github.com/go-playground/locales/en"
	"github.com/go-playground/validator/v10"
)

type Errors struct {
	Status        int             `json:"status"`
	Version       string          `json:"version"`
	Title         string          `json:"title"`
	InvalidParams []InvalidParams `json:"invalid_params"`
}
type InvalidParams struct {
	Name   string `json:"name"`
	Reason string `json:"reason"`
}

var validate *validator.Validate

func addTranslation(tag, message string, trans ut.Translator) error {
	return validate.RegisterTranslation(tag, trans, func(ut ut.Translator) error {
		return ut.Add(tag, "{0} "+message, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(tag, fe.Field())
		return t
	})
}
func translateOverride(i interface{}, trans ut.Translator) validator.ValidationErrors {
	validate = validator.New()
	_ = addTranslation("required", "must have a value", trans)
	_ = addTranslation("email", "invalid", trans)
	err := validate.Struct(i)
	if err != nil {
		errs := err.(validator.ValidationErrors)
		return errs
	}
	return nil
}
func Validate(i interface{}, error error) error {
	trans, _ := ut.New(en.New(), en.New()).GetTranslator("en")
	errors := Errors{}
	errors.Version = os.Getenv("VERSION")
	errors.Status = http.StatusBadRequest
	if error != nil {
		index := strings.Index(error.Error(), "invalid")
		if index != -1 {
			errors.Title = "Your request parameters didn't validate. [JSON syntax error]"
			b, _ := json.Marshal(&errors)
			return systemError.New(string(b))
		}
		errors.Title = error.Error()
		b, _ := json.Marshal(&errors)
		return systemError.New(string(b))
	}
	errors.Title = "Your request parameters didn't validate."
	errs := translateOverride(i, trans)
	if len(errs) == 0 {
		return nil
	}
	for _, e := range errs {
		errors.InvalidParams = append(errors.InvalidParams, InvalidParams{
			Reason: e.Translate(trans),
		})
	}
	b, _ := json.Marshal(&errors)
	return systemError.New(string(b))
}
