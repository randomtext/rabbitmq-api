package utils

import (
	"bytes"
	"encoding/json"
	"gitlab.com/random-text/rabbitMQ/src/payloads"
)

func Serialize(p *payloads.Text) ([]byte, error) {
	var b bytes.Buffer
	encoder := json.NewEncoder(&b)
	err := encoder.Encode(p)
	return b.Bytes(), err
}

func Deserialize(b []byte) (*payloads.Text, error) {
	var t *payloads.Text
	buf := bytes.NewBuffer(b)
	decoder := json.NewDecoder(buf)
	err := decoder.Decode(&t)
	return t, err
}
