package usecase

import (
	"context"
	"github.com/go-redis/redis/v8"
	"gitlab.com/random-text/rabbitMQ/src/payloads"
	"gitlab.com/random-text/rabbitMQ/src/server"
	"gitlab.com/random-text/rabbitMQ/src/utils"
	"go.mongodb.org/mongo-driver/bson"
	"time"
)

type usecase struct {
	R      server.Repository
	RedisC *redis.Client
}

func New(r server.Repository, redisC *redis.Client) server.UseCase {
	return &usecase{R: r, RedisC: redisC}
}

func (uc *usecase) NewText(p *payloads.Text) {
	update := bson.M{ "rabbitmq_status": "RabbitMQ ok" }
	err := uc.updateTextStatus(p, update)
	if err != nil {
		utils.Log(true, err.Error())
		return
	}
	time.Sleep(time.Second)
	err = uc.publishRedis(p)
	if err != nil {
		utils.Log(true, err.Error())
		update := bson.M{ "redis_status": "Redis sent fail" }
		err := uc.updateTextStatus(p, update)
		if err != nil {
			utils.Log(true, err.Error())
			return
		}
		return
	}
}

func (uc *usecase) updateTextStatus(p *payloads.Text, update bson.M) error {
	filter := bson.M{ "_id": p.ID }
	return uc.R.UpdateTextStatus(filter, update)
}

func (uc *usecase) publishRedis(p *payloads.Text) error {
	textBytes, err := utils.Serialize(p)
	if err != nil {
		return err
	}
	_, err = uc.RedisC.Publish(context.TODO(), "random-texts", textBytes).Result()
	if err != nil {
		return err
	}
	return nil
}
