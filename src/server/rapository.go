package server

import "go.mongodb.org/mongo-driver/bson"

type Repository interface {
	UpdateTextStatus(filter, update bson.M) error
}
