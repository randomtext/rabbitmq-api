package rabbitmq

import (
	"github.com/streadway/amqp"
	"gitlab.com/random-text/rabbitMQ/src/server"
	"gitlab.com/random-text/rabbitMQ/src/utils"
)

type handler struct {
	UC            server.UseCase
	RabbitMQCh    *amqp.Channel
	RabbitMQQueue amqp.Queue
}

func New(uc server.UseCase, rabbitMQCh *amqp.Channel, rabbitMQQueue amqp.Queue) *handler {
	h := &handler{UC: uc, RabbitMQCh: rabbitMQCh, RabbitMQQueue: rabbitMQQueue}
	h.startConsumer()
	return h
}

func (h *handler) startConsumer() {
	msgs, err := h.RabbitMQCh.Consume(
		h.RabbitMQQueue.Name, // queue
		"",     // consumer
		true,   // auto ack
		false,  // exclusive
		false,  // no local
		false,  // no wait
		nil,    // args
	)
	if err != nil {
		panic(err)
	}
	go func() {
		for d := range msgs {
			t, err := utils.Deserialize(d.Body)
			if err != nil {
				utils.Log(true, err.Error())
			}
			err = utils.Validate(t, nil)
			if err != nil {
				utils.Log(true, err.Error())
				continue
			}
			h.UC.NewText(t)
			utils.Log(false, "NewText")
		}
	}()
}


