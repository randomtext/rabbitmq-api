package server

import (
	"gitlab.com/random-text/rabbitMQ/src/payloads"
)

type UseCase interface {
	NewText(p *payloads.Text)
}
