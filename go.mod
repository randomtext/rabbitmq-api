module gitlab.com/random-text/rabbitMQ

go 1.16

require (
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.6.1
	github.com/go-redis/redis/v8 v8.10.0
	github.com/streadway/amqp v1.0.0
	go.mongodb.org/mongo-driver v1.5.3
)
